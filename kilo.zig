const std = @import("std");

var original_termios: std.os.termios = undefined;

const stdin: std.fs.File.Reader = std.io.getStdIn().reader();
const stdout: std.fs.File.Writer = std.io.getStdOut().writer();

const SCREEN_ERASE = "\x1b[2J";
const ZERO_CURSOR = "\x1b[H";

fn disableRawMode() !void {
    try std.os.tcsetattr(std.os.STDIN_FILENO, .FLUSH, original_termios);
}

fn multiOr(comptime T: type, vals: anytype) T {
    var x: T = 0;
    inline for (std.meta.fields(@TypeOf(vals))) |field|
        x |= @field(vals, field.name);
    return x;
}

fn enableRawMode() !void {
    original_termios = try std.os.tcgetattr(std.os.STDIN_FILENO);
    var raw = original_termios;

    raw.iflag &= ~multiOr(@TypeOf(raw.iflag), .{
        std.os.system.IXON,
        std.os.system.ICRNL,
        std.os.system.BRKINT,
        std.os.system.INPCK,
        std.os.system.ISTRIP,
    });
    raw.oflag &= ~@as(@TypeOf(raw.oflag), std.os.system.OPOST);
    raw.lflag &= ~multiOr(@TypeOf(raw.lflag), .{
        std.os.system.ECHO,
        std.os.system.ICANON,
        std.os.system.ISIG,
        std.os.system.IEXTEN,
    });
    raw.cflag |= std.os.system.CS8;
    raw.cc[std.os.system.V.MIN] = 0;
    raw.cc[std.os.system.V.TIME] = 1;

    try std.os.tcsetattr(std.os.STDIN_FILENO, .FLUSH, raw);
}

fn ctrlKey(key: u8) u8 {
    return key & 0x1F;
}

fn clearScreen() !void {
    for ([_][]const u8{ SCREEN_ERASE, ZERO_CURSOR }) |msg|
        try stdout.writeAll(msg);
}

fn drawRows() !void {
    var y: usize = 0;
    while (y < 24) : (y += 1) try stdout.writeAll("~\r\n");
}

fn refreshScreen() !void {
    try clearScreen();
    try drawRows();
    try stdout.writeAll(ZERO_CURSOR);
}

fn readKey() !u8 {
    return stdin.readByte() catch |e|
        if (e == error.EndOfStream) '\x00' else return e;
}

fn processKey() !void {
    const key = try readKey();
    switch (key) {
        ctrlKey('q') => return error.Quit,
        else => {},
    }
}

pub fn main() anyerror!void {
    try enableRawMode();
    defer clearScreen() catch unreachable;
    defer disableRawMode() catch unreachable;

    while (true) {
        try refreshScreen();
        processKey() catch |e|
            if (e == error.Quit) break else return e;
    }
}
