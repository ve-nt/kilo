const std = @import("std");

const Builder = std.build.Builder;
const Step = std.build.Step;

pub fn build(b: *Builder) void {
    const target = b.standardTargetOptions(.{});
    const mode = b.standardReleaseOptions();
    const output_dir = "bin";

    const kilo_zig = b.addExecutable("kilo-zig", "kilo.zig");
    kilo_zig.setTarget(target);
    kilo_zig.setBuildMode(mode);
    kilo_zig.setOutputDir(output_dir);
    kilo_zig.install();

    const kilo_c = b.addExecutable("kilo-c", "kilo.c");
    kilo_c.setTarget(target);
    kilo_c.setBuildMode(mode);
    kilo_c.linkLibC();
    kilo_c.setOutputDir(output_dir);
    kilo_c.install();

    const clean_step = b.step("clean", "Clean build artifacts");
    clean_step.makeFn = struct {
        fn make(_: *Step) anyerror!void {
            const cwd = std.fs.cwd();
            try cwd.deleteTree(output_dir);
            try cwd.deleteTree("zig-out");
            try cwd.deleteTree("zig-cache");
        }
    }.make;
}
