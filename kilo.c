#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>

#define SCREEN_ERASE "\x1b[2J"
#define ZERO_CURSOR "\x1b[H"

#define CTRL_KEY(k) (k & 0x1f)

static void die(char*);

static clearScreen(void)
{
    const char* m[] = {SCREEN_ERASE, ZERO_CURSOR};
    for (size_t i = 0; i < sizeof(m) / sizeof(m[0]); ++i)
        if (write(STDOUT_FILENO, m[i], strlen(m[i])) <= 0) die("refreshScreen");
}

static void die(char* s)
{
    clearScreen();
    perror(s);
    exit(EXIT_FAILURE);
}

static struct termios original_termios;

static void enableRawMode(void)
{
    if (tcgetattr(STDIN_FILENO, &original_termios) == -1) die("enableRawMode, tcgetattr");
    struct termios raw = original_termios;
    raw.c_iflag &= ~(IXON | ICRNL | BRKINT | INPCK | ISTRIP);
    raw.c_oflag &= ~(OPOST);
    raw.c_cflag |= CS8;
    raw.c_lflag &= ~(ECHO | ICANON | ISIG | IEXTEN);
    raw.c_cc[VMIN] = 0;
    raw.c_cc[VTIME] = 1;
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) == -1) die("enableRawMode, tcsetattr");
}

static void disableRawMode(void)
{
    if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &original_termios) == -1)
        die("disableRawMode, tcsetattr");
}

static void drawRows()
{
    for (int y = 0; y < 24; ++y) write(STDOUT_FILENO, "~\r\n", 3);
}

static void refreshScreen()
{
    clearScreen();
    drawRows();
    write(STDOUT_FILENO, ZERO_CURSOR, strlen(ZERO_CURSOR));
}

static char readKey()
{
    int nread;
    char c;
    while ((nread = read(STDIN_FILENO, &c, 1)) != 1)
        if (nread == -1 && errno != EAGAIN) die("readKey, read");
    return c;
}

static void processKey()
{
    char c = readKey();
    switch (c) {
    case CTRL_KEY('q'):
        clearScreen();
        exit(0);
        break;
    }
}

int main(int argc, char* argv[])
{
    enableRawMode();
    atexit(disableRawMode);
    while (true) {
        refreshScreen();
        processKey();
    }
    return 0;
}
